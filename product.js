export const generateProduct = (product) => `
        <div class="catalog__product">
            <div class="catalog__photo">
              <img src="${product.image}"/>
            </div>
            <div class="catalog__title">
              <p>
                ${product.title}
              </p>
            </div>
            <div class="catalog__price">
              <span>Cost: ${product.price}$</span>
            </div>
          </div>`;

export const generateProductsHTML = (productsList) => {
    let productHTML = "";
    for (const product of productsList) {
        productHTML += generateProduct(product);
    }
    return productHTML;
};
