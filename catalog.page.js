import { products } from "./API.js";
import { generateProductsHTML } from "./product.js";

const catalog = document.getElementById("catalog");

const fillUpCatalog = async (sort = null) => {
    const productsList = await products(sort);
    catalog.innerHTML = generateProductsHTML(productsList);
};
fillUpCatalog();

// sort
const sort = document.getElementById("sort");
sort.addEventListener("change", () => {
    fillUpCatalog(sort.value);
});

//  search
const searchQuery = document.getElementById("searchQuery");
const searchButton = document.getElementById("searchButton");
searchButton.addEventListener("click", () => {
    products(sort).then((result) => {
        const filterProducts = result.filter(
            (product) => product.title.indexOf(searchQuery.value) != -1
        );
        catalog.innerHTML = generateProductsHTML(filterProducts);
    });
});
